import 'package:flutter/material.dart';
import 'package:security_mobile/Screens/home.dart';
import 'package:security_mobile/Screens/contact.dart';
import 'package:security_mobile/Screens/devicedetails.dart';
import 'package:security_mobile/Screens/location.dart';
import 'package:security_mobile/Screens/personaldetails.dart';
import 'package:security_mobile/Screens/policestationdetails.dart';

void main(List<String> args) {
  runApp(new MaterialApp(
    home: new Home(),

     initialRoute: '/',
  routes: {
    '/home': (context) => Home(),
    '/contact': (context) => ContactApp(),
    '/device': (context) => DeviceApp(),
    '/location':(context) => Location(),
    '/personal':(context) => PersonalDetails(),
    '/policestation':(context) => PoliceStation()
  },

  ));
}
