import 'package:flutter/material.dart';

void main() => runApp(new ContactApp());

class ContactApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Home Security",
      debugShowCheckedModeBanner: false,
      home: Center(child: new ContactPage(title: 'Contact Details')),
    );
  }
}

class ContactPage extends StatefulWidget {
  ContactPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ContactPageState createState() => new _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  FocusNode node1 = FocusNode();
  FocusNode node2 = FocusNode();
  FocusNode node3 = FocusNode();
  FocusNode node4 = FocusNode();
  FocusNode node5 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Center(
          child: new Text(
            widget.title,
            style: TextStyle(color: Colors.black, fontSize: 24.0),
          ),
        ),
        backgroundColor: Colors.grey[100],
      ),
      body: Container(
        child: new ListView(
          children: <Widget>[
            Container(
              color: Colors.grey[200],
              width: 50.0,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Icon(
                        Icons.lightbulb_outline,
                        color: Colors.amber,
                        size: 48.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey[200],
                      height: 70.0,
                      width: 300.0,
                      child: Center(
                        child: Text(
                          'Emergency information will be communicated to your relation and neighbour. '
                              'please provide valid details',
                          maxLines: 3,
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.black,
                              fontStyle: FontStyle.italic),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.grey[100],
              height: 70.0,
              width: 100.0,
              child: Center(
                child: Text(
                  'Emergency Contact',
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                focusNode: node1,
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Name ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                focusNode: node2,
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Mobile No ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                focusNode: node3,
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                decoration: InputDecoration(
                  hintText: "Relationship  ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.grey[100],
              height: 70.0,
              width: 100.0,
              child: Center(
                child: Text(
                  'Neighbour Contact',
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                focusNode: node4,
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                decoration: InputDecoration(
                  hintText: "Name ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                focusNode: node5,
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Mobile No ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 60.0, right: 60.0, top: 16.0, bottom: 16.0),
              child: RaisedButton(
                child: const Text(
                  'Next',
                  style: TextStyle(fontSize: 16.0),
                ),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                elevation: 4.0,
                splashColor: Colors.grey[100],
                onPressed: () {
                  // Perform some action
                  Navigator.of(context).pushNamed('/policestationdetails');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
