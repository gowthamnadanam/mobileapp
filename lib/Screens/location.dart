import 'package:flutter/material.dart';

void main() => runApp(new Location());

class Location extends StatelessWidget{

    @override
    Widget build(BuildContext context){
      return new MaterialApp(
        title: 'Home Security',
        debugShowCheckedModeBanner: false,
        home: Center(child: new LocationPage(title: 'Location Details')),
      );
  }
}

class LocationPage extends StatefulWidget{
  LocationPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  LocationDetails createState() => new LocationDetails(); 
}

class LocationDetails extends State<LocationPage>{
  @override
    Widget build(BuildContext context){
      return new Scaffold(
        appBar: new AppBar(
          title: Center(
            child: new Text(
              widget.title,
              style: TextStyle(color: Colors.black, fontSize: 24.0),
            ),
          ),
          backgroundColor: Colors.grey[100],
        ),
         body: Container(
        child: new ListView(
          children: <Widget>[
            Container(
              color: Colors.grey[200],
              width: 50.0,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Icon(
                        Icons.lightbulb_outline,
                        color: Colors.amber,
                        size: 48.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey[200],
                      height: 70.0,
                      width: 300.0,
                      child: Center(
                        child: Text(
                          'Emergency information will be communicated'
                              'to your relation and neighbour. Please provide'
                              'valid details.',
                          maxLines: 3,
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.black,
                              fontStyle: FontStyle.italic),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Address 1 ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Address 2 ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
                 padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "City ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
                 padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "State ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
                 padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Country ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Pin Code ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 60.0, right: 60.0, top: 16.0, bottom: 16.0),
              child: RaisedButton(
                child: const Text(
                  'Next',
                  style: TextStyle(fontSize: 16.0),
                ),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                elevation: 4.0,
                splashColor: Colors.grey[100],
                onPressed: () {
                  // Perform some action
                  Navigator.of(context).pushNamed('/policestationdetails');
                },
              ),
            ),
          ],
        ),
      ),
      );
    }
}