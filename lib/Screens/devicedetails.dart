import 'package:flutter/material.dart';

void main() => runApp(new DeviceApp());

class DeviceApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Home Security",
      debugShowCheckedModeBanner: false,
      home: Center(child: new DevicePage(title: 'Device Details')),
    );
  }
}

class DevicePage extends StatefulWidget {
  DevicePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DevicePageState createState() => new _DevicePageState();
}

class _DevicePageState extends State<DevicePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Center(
          child: new Text(
            widget.title,
            style: TextStyle(color: Colors.black, fontSize: 24.0),
          ),
        ),
        backgroundColor: Colors.grey[100],
      ),
      body: Container(
      )
    );
  }
}