import 'package:flutter/material.dart';


void main() => runApp(new PersonalDetails());

class PersonalDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Home Security",
      debugShowCheckedModeBanner: false,
      home: Center(child: new MyHomePage(title: 'Personal Details')),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MyPersonalDetails createState() => new MyPersonalDetails();
}

class MyPersonalDetails extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Center(
          child: new Text(
            widget.title,
            style: TextStyle(color: Colors.black, fontSize: 24.0),
          ),
        ),
        backgroundColor: Colors.grey[100],
      ),
      body: Container(
        child: new ListView(
          children: <Widget>[
            Container(
              color: Colors.grey[200],
              width: 50.0,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Icon(
                        Icons.lightbulb_outline,
                        color: Colors.amber,
                        size: 48.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey[200],
                      height: 70.0,
                      width: 300.0,
                      child: Center(
                        child: Text(
                          'Please provide valid details. '
                              'we will not share with others. '
                              'To verify, we will send OTP to your primary mobile number. ',
                          maxLines: 3,
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.black,
                              fontStyle: FontStyle.italic),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "First Name ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Last Name ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Primary mobile no",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Secondary mobile no ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "OTP",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 60.0, right: 60.0, top: 16.0, bottom: 16.0),
              child: RaisedButton(
                child: const Text(
                  'Next',
                  style: TextStyle(fontSize: 16.0),
                ),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                elevation: 4.0,
                splashColor: Colors.grey[100],
                onPressed: () {
                  // Perform some action
                 
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
