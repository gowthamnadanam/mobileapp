import 'package:flutter/material.dart';

void main() => runApp(new PoliceStation());

class PoliceStation extends StatelessWidget{
    @override
    Widget build(BuildContext context){
      return new MaterialApp(
        title: 'Home Security',
        debugShowCheckedModeBanner: false,
        home: Center(child: new PoliceStationPage(title: 'Police Station Details')),
      );
  }
}

class PoliceStationPage extends StatefulWidget{
  PoliceStationPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  PoliceStationDetails createState() => new PoliceStationDetails(); 
}

class PoliceStationDetails extends State<PoliceStationPage>{
  @override
    Widget build(BuildContext context){
      return new Scaffold(
        appBar: new AppBar(
          title: Center(
            child: new Text(
              widget.title,
              style: TextStyle(color: Colors.black, fontSize: 24.0),
            ),
          ),
          backgroundColor: Colors.grey[100],
        ),
         body: Container(
        child: new ListView(
          children: <Widget>[
            Container(
              color: Colors.grey[200],
              width: 50.0,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Icon(
                        Icons.lightbulb_outline,
                        color: Colors.amber,
                        size: 48.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey[200],
                      height: 70.0,
                      width: 300.0,
                      child: Center(
                        child: Text(
                          'Emergency information will be communicated'
                              'to your nearest police station. Please provide'
                              'valid details.',
                          maxLines: 3,
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.black,
                              fontStyle: FontStyle.italic),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                style: TextStyle(fontSize: 24.0, color: Colors.black),
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "police station number ",
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                  ),
                ),
              ),
              
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 60.0, right: 60.0, top: 16.0, bottom: 16.0),
              child: RaisedButton(
                child: const Text(
                  'Submit',
                  style: TextStyle(fontSize: 16.0),
                ),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                elevation: 4.0,
                splashColor: Colors.grey[100],
                onPressed: () {
                  // Perform some action
                 
                },
              ),
            ),
          ],
        ),
      ),
      );
    }
}