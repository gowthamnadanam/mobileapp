import 'package:flutter/material.dart';
import './location.dart';
import './contact.dart';
import './devicedetails.dart';


class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _HomeState();
  }
}

class _HomeState extends State<Home> {

  bool value1 =  true;
  bool value2 = false;
  bool value3 = true;
  bool value4 = false;
  bool value5 = true;
  

  void onChangedValue2(bool value){
    setState(() {
      value2 = value;
    });
  }

  void onChangedValue3(bool value){
    setState(() {
      value3 = value;
    });
  }

  void onChangedValue(bool value){
    setState(() {
      value4 = value;
    });
  }

  void onChangedValue5(bool value){
    setState(() {
      value5 = value;
    });
  }

  String name = '';
  void onChangeValue(String value){
    setState(() {
       name = 'on changed: $value';
    });
  }

  void onSubmitValue(String value){
    setState(() {
       name = 'on Submitted: $value';
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: new Text('Home',) ,
      ),
      body: new Container(

        child: new ListView(
          children: <Widget>[
           
             new ListTile(
               
              title: new Text("System Status",style: new TextStyle(fontSize: 23.0),),
              trailing: new Switch(value: value4, onChanged: onChangedValue, activeColor: Colors.green,),
              
            ),

new Divider(),


             new ListTile(
              title: new Text("Location(s)",style: new TextStyle(fontSize: 23.0),),
              trailing: new Icon(Icons.add),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new Location()));
              },
            ),
new Divider(),

            new ListTile(
              title: new Text("Contact",style: new TextStyle(fontSize: 23.0),),
              trailing: new Icon(Icons.add),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new ContactApp()));
              },
            ),

new Divider(),

            new ListTile(
              title: new Text("Devices",style: new TextStyle(fontSize: 23.0),),
              trailing: new Icon(Icons.add),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new DeviceApp()));
              },
            ),
          ],          
        ),
      ),      
    );
  }
}